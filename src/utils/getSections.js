import { faHome, faUser, faTh, faFile, faEnvelope } from '@fortawesome/free-solid-svg-icons';

export function getSections() {
    const sections = [
        { id: 'top', name: 'Intro', icon: faHome },
        { id: 'about', name: 'About Me', icon: faUser },
        { id: 'portfolio', name: 'Portfolio', icon: faTh },
        { id: 'resume', name: 'Resume', icon: faFile },
        { id: 'contact', name: 'Contact', icon: faEnvelope }
    ];
    return sections;
}
