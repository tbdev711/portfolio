import { faGitlab, faLinkedin } from '@fortawesome/free-brands-svg-icons';
import { faEnvelopeOpen } from '@fortawesome/free-solid-svg-icons';

export function getSocialLinks() {
    const socialLinks = [
        { icon: faGitlab, name: 'GitLab', url: 'https://gitlab.com/users/tbdev711/projects' },
        { icon: faLinkedin, name: 'LinkedIn', url: 'https://linkedin.com/in/timbwoods' },
        { icon: faEnvelopeOpen, name: 'Email', url: 'mailto:tb.dev711@gmail.com' }
    ];
    return socialLinks;
}
