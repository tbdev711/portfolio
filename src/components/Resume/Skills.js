import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faDesktop, faRunning, faSync, faFlask, faUser } from '@fortawesome/free-solid-svg-icons';

const Skills = () => (
    <>
        <section>
            <div className="container">
                <h3 className="header"> Experience</h3>
                <div className="row">
                    <div className=" col-12 col-sm">
                        <FontAwesomeIcon icon={faDesktop} />
                        <h4> Development</h4>
                        <table>
                            <tbody>
                                <tr>
                                    <td> HTML</td>
                                    <td> JavaScript</td>
                                    <td> CSS </td>
                                </tr>
                                <tr>
                                    <td> Sass </td>
                                    <td> Redux Toolkit </td>
                                    <td> Postman </td>
                                </tr>
                                <tr>
                                    <td> npm </td>
                                    <td> Bootstrap</td>
                                    <td> Storybook </td>
                                </tr>
                                <tr>
                                    <td> Babel </td>
                                    <td>Webpack </td>
                                    <td>React </td>
                                </tr>
                                <tr>
                                    <td>Express </td>
                                    <td>Gatsby </td>
                                    <td>Redux </td>
                                </tr>
                                <tr>
                                    <td>Balsamiq Mockups</td>
                                    <td>Adobe Illustrator </td>
                                    <td>Adobe Photoshop </td>
                                </tr>
                                <tr>
                                    <td>Trello </td>
                                    <td>ES-Lint</td>
                                    <td>Husky </td>
                                </tr>
                                <tr>
                                    <td>Jest</td>
                                    <td>React Testing Library</td>
                                    <td>Prettier </td>
                                </tr>
                                <tr>
                                    <td>SonarQube </td>
                                    <td>Docker</td>
                                    <td>Google Cloud Container Registry</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-3 col-sm">
                        <FontAwesomeIcon icon={faRunning} />
                        <h4>Agile Methodologies</h4>
                        <p>Scrum & Kanban </p>
                    </div>
                    <div className="col-3 col-sm">
                        <FontAwesomeIcon icon={faSync} />
                        <h4> CI / CD </h4>
                        <p>GitLab & Google Cloud Run</p>
                    </div>
                    <div className="col-3 col-sm">
                        <FontAwesomeIcon icon={faFlask} />
                        <h4> Code Quality </h4>
                        <p> Black box & White box Testing</p>
                    </div>
                    <div className="col-3 col-sm">
                        <FontAwesomeIcon icon={faUser} />
                        <h4> Engagement </h4>
                        <p> Client & Stakeholder</p>
                    </div>
                </div>
            </div>
        </section>
    </>
);

export default Skills;
