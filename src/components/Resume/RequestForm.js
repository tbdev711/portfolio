import React from 'react';

const RequestForm = () => (
    <form method="post" action="https://usebasin.com/f/b8d4044e4853">
        <div className="row">
            <div className="col-6 col-12-mobile">
                <input type="text" name="name" placeholder="First name" aria-label="First Name" />
            </div>
            <div className="col-6 col-12-mobile">
                <input type="text" name="lname" placeholder="Last name" aria-label="Last Name" />
            </div>
            <div className="col-12 col-12-mobile">
                <input type="text" name="email" placeholder="Email" aria-label="Email" />
            </div>
            <div className="col-12 col-12-mobile">
                <input type="text" name="company" placeholder="Company" aria-label="Company" />
            </div>
            <div className="col-12">
                <textarea
                    name="message"
                    placeholder="Message"
                    aria-label="Message"
                    value="Hi Tim! I would like an access link to view your resume. Thanks!"
                />
            </div>
            <div className="col-12">
                <input type="submit" value="Request Access" aria-label="Submit" />
            </div>
        </div>
    </form>
);

export default RequestForm;
