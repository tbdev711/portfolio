import React from 'react';
import PropTypes from 'prop-types';
import RequestForm from './RequestForm';

function ResumeModal({ onClose }) {
    return (
        <div key="modal">
            <div className="modal is-active">
                <div className="modal-background">
                    <div className="modal-card">
                        <header>
                            <button onClick={onClose} className="modal-close is-large" aria-label="close"></button>
                        </header>

                        <section className="modal-card-body">
                            <RequestForm />
                        </section>
                    </div>
                </div>
            </div>
        </div>
    );
}
ResumeModal.propTypes = {
    onClose: PropTypes.func.isRequired
};
export default ResumeModal;
