import React from 'react';
import { Link } from 'gatsby';
import PropTypes from 'prop-types';

export default function Header({ title, heading, avatar }) {
    return (
        <div id="logo">
            <Link to="/">
                <div className="image avatar48">
                    <img height="512px" src={avatar} alt="" />
                </div>
                <div>
                    <h1 id="title">{title}</h1>
                    <p>{heading}</p>
                </div>
            </Link>
        </div>
    );
}
Header.propTypes = {
    title: PropTypes.string.isRequired,
    heading: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired
};
