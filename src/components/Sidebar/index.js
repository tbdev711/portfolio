import React, { useState } from 'react';
import Footer from './Footer';
import Header from './Header';
import Nav from './Nav';
import TopNav from './TopNav';
import config from '../../../config';
import { getSocialLinks } from '../../utils/getIcons';
import PropTypes from 'prop-types';

const pic = require('../../assets/images/avatar.svg');

export default function SideBar({ sections = [] }) {
    const [isHeaderOpen, setIsHeaderOpen] = useState(false);
    const links = getSocialLinks();
    return (
        <div className={`${isHeaderOpen ? 'header-visible' : ' '}`}>
            <TopNav title={config.authorName} onMenuClick={() => setIsHeaderOpen(!isHeaderOpen)} />
            <div id="header">
                <div className="top">
                    <Header avatar={pic} title={config.authorName} heading={config.heading} />
                    <Nav sections={sections} />
                </div>
                <Footer socialLinks={links} />
            </div>
        </div>
    );
}

SideBar.propTypes = {
    sections: PropTypes.array.isRequired
};
