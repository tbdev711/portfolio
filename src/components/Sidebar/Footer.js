import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default function Footer({ socialLinks = [] }) {
    return (
        <div className="bottom">
            <ul className="icons">
                {socialLinks.map((social) => {
                    const { icon, name, url } = social;
                    return (
                        <li key={url}>
                            <a href={url} target="_blank" rel="noopener noreferrer" className={`icon ${icon}`}>
                                <FontAwesomeIcon icon={icon} />
                            </a>
                        </li>
                    );
                })}
            </ul>
        </div>
    );
}

Footer.propTypes = {
    socialLinks: PropTypes.array.isRequired
};
