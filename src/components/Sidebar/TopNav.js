import React from 'react';
import PropTypes from 'prop-types';

export default function TopNav({ onMenuClick = () => {} }) {
    return (
        <div id="headerToggle">
            <a
                href="/#"
                className="toggle"
                onClick={(e) => {
                    e.preventDefault();
                    onMenuClick();
                }}
            ></a>
        </div>
    );
}

TopNav.propTypes = {
    onMenuClick: PropTypes.func.isRequired
};
