import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';

import '../assets/sass/main.scss';

class Layout extends Component {
    render() {
        const { children } = this.props;
        return (
            <>
                <Helmet
                    title="Portfolio"
                    meta={[
                        { name: 'description', content: 'Eventually' },
                        { name: 'keywords', content: 'site, web' }
                    ]}
                ></Helmet>
                <html lang="en">{children}</html>
            </>
        );
    }
}

Layout.propTypes = {
    children: PropTypes.node.isRequired
};

export default Layout;
