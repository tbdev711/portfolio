import React from 'react';

export default function Contact() {
    return (
        <div className="container">
            <header>
                <h2>Contact</h2>
            </header>

            <p> Questions? Comments? Let&apos;s connect! </p>

            <form method="post" action="https://usebasin.com/f/28134338aca9">
                <div className="row">
                    <div className="col-6 col-12-mobile">
                        <input type="text" name="name" placeholder="Name" aria-label="Name" />
                    </div>
                    <div className="col-6 col-12-mobile">
                        <input type="text" name="email" placeholder="Email" aria-label="Email" />
                    </div>
                    <div className="col-12">
                        <textarea name="message" placeholder="Message" aria-label="Message" />
                    </div>
                    <div className="col-12">
                        <input type="submit" value="Send Message" aria-label="Submit" />
                    </div>
                </div>
            </form>
        </div>
    );
}
