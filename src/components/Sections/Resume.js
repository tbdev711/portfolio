import React, { useState } from 'react';
import RequestModal from '../Resume/RequestModal';

export default function Resume() {
    const [showAlert, setShowAlert] = useState(false);

    return (
        <div>
            <section>
                <div className="container">
                    <p>
                        Early on we learn that life is, as they say, what we make of it. Either play with the hand you
                        are dealt or fold. My work ethic and leadership mentality are the culmination of continuous
                        self-growth, the disciple of the Martial Arts, the ability to accept and inspect failure, and
                        the peers/mentors that have helped guide me along the way.
                    </p>
                    To request an access link to my resume, available via Google Docs, click &quot;request access&quot;
                </div>

                {showAlert && <RequestModal onClose={() => setShowAlert(false)} />}
                <button key="modal" id="modal" onClick={() => setShowAlert(true)}>
                    <span>
                        <span>Request access</span>
                    </span>
                </button>
            </section>
        </div>
    );
}
