import React from 'react';
import weather from '../../assets/images/weather.png';
import Skills from '../Resume/Skills';

export default function Portfolio() {
    return (
        <div className="container">
            <header>
                <h2>Portfolio</h2>
            </header>

            <div className="row">
                <div className="col-4 col-12-mobile">
                    <article className="item">
                        <a
                            href="https://weather.timothywoods.dev/"
                            target="_blank"
                            rel="noopener noreferrer"
                            className="image fit"
                        >
                            <img src={weather} alt="weather app" />
                        </a>
                        <header>
                            <h3>Weather</h3>
                        </header>
                        TB-Dev Weather v.2.0 is a React based application developed as a way to reintroduce myself to
                        coding. I am excited to share this app with you and welcome your feedback and suggestions!
                    </article>

                    <article className="item">
                        <header>
                            <h3>Code Challenges</h3>
                        </header>
                    </article>
                </div>

                <div className="col-8 col-12-mobile">
                    <div style={{ height: '100%' }}>
                        <Skills />
                    </div>
                </div>
            </div>
        </div>
    );
}
