import React from 'react';

export default function Intro() {
    return (
        <div className="container">
            <header>
                <h2 className="alt">
                    Hi! I&apos;m <strong>Tim,</strong>
                    <br />
                    IT Specialist & Web Developer
                </h2>
                <p>Feel free to have a look around</p>
            </header>
        </div>
    );
}
