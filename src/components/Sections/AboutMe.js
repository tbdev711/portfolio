import React from 'react';

export default function AboutMe() {
    return (
        <div className="container">
            <div>
                As a person who believes in self growth as a life-long journey, constantly seeking new challenges is my
                hobby. As the technology field grows at an accelerated rate, each day we make strides toward achieving
                what was once thought impossible. This fuels my passion to continue this important work for the
                advancement of our future.
            </div>
        </div>
    );
}
