import React from 'react';

export default function PageFooter() {
    return (
        <div id="footer">
            <ul className="copyright">
                <li> 2020 </li>
                <li> &copy; TB-Dev. </li>
                <li> All rights reserved. </li>
            </ul>
        </div>
    );
}
