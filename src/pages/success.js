import React from 'react';
import { Link } from 'gatsby';
import Layout from '../components/Layout';
import SideBar from '../components/Sidebar';

const IndexPage = () => (
    <Layout>
        <SideBar />
        <div id="wrapper">
            <div id="main">
                <section>
                    <div className="container">
                        <section>
                            <h1>
                                Your request has been submitted. You will receive an email shortly containing a link to
                                view my resume.
                            </h1>
                            <Link to="/#resume">
                                <button>Return</button>
                            </Link>
                        </section>
                    </div>
                </section>
            </div>
        </div>
    </Layout>
);

export default IndexPage;
