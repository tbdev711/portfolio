import React from 'react';
import AboutMe from '../components/Sections/AboutMe';
import Contact from '../components/Sections/Contact';
import Intro from '../components/Sections/Intro';
import Layout from '../components/Layout';
import PageFooter from '../components/PageFooter';
import Portfolio from '../components/Sections/Portfolio';
import Resume from '../components/Sections/Resume';

import SideBar from '../components/Sidebar';
import { getSections } from '../utils/getSections';

const sections = getSections();

const IndexPage = () => (
    <Layout>
        <SideBar sections={sections} />

        <div id="main">
            <section id="top" className="one dark cover">
                <Intro />
            </section>

            <section id="about" className="two">
                <AboutMe />
            </section>

            <section id="portfolio" className="three">
                <Portfolio />
            </section>

            <section id="resume" className="four">
                <Resume />
            </section>

            <section id="contact" className="five">
                <Contact />
            </section>
        </div>

        <PageFooter />
    </Layout>
);

export default IndexPage;
