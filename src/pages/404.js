import React from 'react';

import Layout from '../components/Layout';
import SideBar from '../components/Sidebar';
import { Link } from 'gatsby';

const IndexPage = () => (
    <Layout>
        <SideBar />
        <div id="wrapper">
            <div id="main">
                <section>
                    <div className="container">
                        <section>
                            <h1>NOT FOUND</h1>
                            <p>Not a valid URL</p>
                            <Link to="/#resume">
                                <button>Return</button>
                            </Link>
                        </section>
                    </div>
                </section>
            </div>
        </div>
    </Layout>
);

export default IndexPage;
