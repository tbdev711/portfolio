FROM node:latest

WORKDIR /usr/src/app

COPY package*.json ./

# Install production dependencies.
RUN npm i -g gatsby-cli
RUN npm install

# Copy local code to the container image.
COPY . ./

RUN gatsby build

ENV PORT 8080

CMD gatsby serve -H 0.0.0.0 -p $PORT
