module.exports = {
    siteTitle: 'Portfolio',
    manifestName: 'Portfolio',
    manifestShortName: 'Landing',
    manifestStartUrl: '/',
    manifestBackgroundColor: '#663399',
    manifestThemeColor: '#663399',
    manifestDisplay: 'standalone',
    pathPrefix: `/`,
    authorName: 'Timothy Woods',
    heading: 'Web Developer'
};
